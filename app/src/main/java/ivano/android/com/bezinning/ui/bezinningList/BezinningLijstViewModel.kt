package ivano.android.com.bezinning.ui.bezinningList

import io.reactivex.Flowable
import javax.inject.Inject
import ivano.android.com.bezinning.data.model.BezinningModel
import ivano.android.com.bezinning.data.repository.BezinningRepository


class BezinningLijstViewModel

@Inject
constructor(
        private val bezinningRepository: BezinningRepository) {


    fun showBezinningLijst(tags: String): Flowable<MutableList<BezinningModel>>? {
        return bezinningRepository.getBezinnings(tags)
    }


}


