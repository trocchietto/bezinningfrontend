package ivano.android.com.bezinning.ui.bezinningList

import android.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ivano.android.com.bezinning.R

class  MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragment:Fragment = MainFragment()

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment, "MainFragment.class")
                .commit()

    }

}
