package ivano.android.com.bezinning.di.module;

import android.content.Context;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ivano.android.com.bezinning.data.datasource.localdatasource.BezinningLocalDataSource;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


@Module(includes = AppModule.class)
public class NetworkModule {

    @Provides
    @Singleton
    HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logInterceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Context context, Cache cache,
                                     HttpLoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Cache provideCache(File file) {
        return new Cache(file, 200*1024*1024);
    }

    @Provides
    @Singleton
    File provideFile(Context context) {
        return new File(context.getCacheDir(), "bezinning_cache");
    }

    @Provides
    @Singleton
    BezinningLocalDataSource provideBezinningLocalDataSource(){
        return new BezinningLocalDataSource();
    }
}
