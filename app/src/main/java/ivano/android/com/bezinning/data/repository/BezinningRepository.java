package ivano.android.com.bezinning.data.repository;


import java.util.List;

import io.reactivex.Flowable;
import ivano.android.com.bezinning.data.model.BezinningModel;



public interface BezinningRepository {
   Flowable<List<BezinningModel>> getBezinnings(String tags);



}
