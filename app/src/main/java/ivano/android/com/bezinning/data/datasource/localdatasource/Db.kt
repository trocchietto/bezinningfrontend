package ivano.android.com.bezinning.data.datasource.localdatasource

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase



@Database(entities = arrayOf(BezinningDb::class),version = 1,exportSchema = false)
 abstract class Db :RoomDatabase(){
    abstract fun bezinningDbDao():BezinningDbDao
}
