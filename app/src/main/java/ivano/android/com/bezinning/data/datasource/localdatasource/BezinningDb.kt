package ivano.android.com.bezinning.data.datasource.localdatasource

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class BezinningDb(

        @PrimaryKey(autoGenerate = true)
        val id: Long,
        val titel: String = "",
        val bodyDb: String = ""

)
