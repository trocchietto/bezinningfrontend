package ivano.android.com.bezinning.di.module;

import android.content.Context;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ivano.android.com.bezinning.data.datasource.remotedatasource.RemoteDataSource;
import ivano.android.com.bezinning.data.datasource.remotedatasource.BezinningAPI;
import ivano.android.com.bezinning.data.datasource.remotedatasource.BezinningRemoteDataSource;
import ivano.android.com.bezinning.data.model.BezinningModel;
import ivano.android.com.bezinning.data.repository.BezinningRepository;
import ivano.android.com.bezinning.data.repository.BezinningRepositoryImpl;


@Module(includes = BezinningApiModule.class)
public class RepositoryModule {

    @Provides
    @Singleton
    public RemoteDataSource<BezinningModel> datasource(BezinningAPI bezinningAPI) {
        return new BezinningRemoteDataSource(bezinningAPI);
    }

    @Provides
    @Singleton
    BezinningRepository provideRepository(RemoteDataSource<BezinningModel> remoteDataSource) {
        return new BezinningRepositoryImpl(remoteDataSource);
    }



}
