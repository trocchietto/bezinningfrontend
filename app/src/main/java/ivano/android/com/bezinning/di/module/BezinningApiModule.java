package ivano.android.com.bezinning.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ivano.android.com.bezinning.data.datasource.remotedatasource.BezinningAPI;
import ivano.android.com.bezinning.BuildConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = NetworkModule.class)
public class BezinningApiModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    BezinningAPI providesApi(Retrofit retrofit){
    return retrofit.create(BezinningAPI.class);
    }
}
