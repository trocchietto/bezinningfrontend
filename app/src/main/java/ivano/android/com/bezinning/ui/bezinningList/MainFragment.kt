package ivano.android.com.bezinning.ui.bezinningList

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.FlowableSubscriber
import javax.inject.Inject
import ivano.android.com.bezinning.R
import ivano.android.com.bezinning.data.model.BezinningModel
import ivano.android.com.bezinning.utils.scheduler.SchedulerProviderImpl
import org.reactivestreams.Subscription




class MainFragment : android.app.Fragment() {



    @Inject
    lateinit var bezinningListViewModel: BezinningLijstViewModel
    @Inject
    lateinit var schedulerProvider: SchedulerProviderImpl



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity.application as BezinningApplication).appComponent!!.inject(this)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.bezinning_fragment, container, false)

        bezinningListViewModel!!.showBezinningLijst("132")
                ?.subscribeOn(schedulerProvider!!.io())
                ?.observeOn(schedulerProvider!!.ui())
                ?.subscribe(object : FlowableSubscriber<List<BezinningModel>> {
            override fun onError(t: Throwable?) {
                Log.d("IVO", "onError: ")
            }

            override fun onComplete() {
                Log.d("IVO", "onComplete: ")
            }

            override fun onSubscribe(s: Subscription) {
                s.request(Long.MAX_VALUE);

            }

            override fun onNext(bezinningModels: List<BezinningModel>?) {

                val JSONResponse = bezinningModels!![0].attribute.body
                val bodyBezinningParsedHtml = Html.fromHtml(JSONResponse)

                val formatJSON = view.findViewById(R.id.bezinning_text) as TextView
                formatJSON.text = bodyBezinningParsedHtml

            }

        })

        return view

    }



}



