package ivano.android.com.bezinning.data.model;


public class Attribute {

    private int  id;
    private String  self;
    private String body;
    private String label;

    public int getId() {
        return id;
    }


    public String getBody() {
        return body;
    }

    public String getSelf() {
        return self;
    }

    public String getLabel() {
        return label;
    }
}
