package ivano.android.com.bezinning.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ivano.android.com.bezinning.utils.scheduler.SchedulerProviderImpl;

@Module
public class SchedulerModule {

    @Provides
    @Singleton
    SchedulerProviderImpl schedulerProvider() {
        return new SchedulerProviderImpl();
    }
}
